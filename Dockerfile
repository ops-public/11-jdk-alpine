FROM alpine:3.12

ENV LANG C.UTF-8
ENV JAVA_HOME="/jdk-11"
ENV PATH=$PATH:${JAVA_HOME}/bin
ENV JAVA_VERSION 11-ea+28
ENV JAVA_ALPINE_VERSION 11~28-1

RUN echo 'nameserver 8.8.8.8' > /etc/resolv.conf
RUN set -x \
        && apk add --no-cache \
        git \
        curl \
        bash
RUN wget https://download.java.net/java/ga/jdk11/openjdk-11_linux-x64_bin.tar.gz && \
        tar -xzf openjdk-11_linux-x64_bin.tar.gz && \
        rm openjdk-11_linux-x64_bin.tar.gz && \
        rm ${JAVA_HOME}/lib/src.zip

CMD ["jshell"]
